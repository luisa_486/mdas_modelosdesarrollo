package com.lasalle.modelosDesarrollo.helloapp.controllers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HelloControllerUnitTest {

    @Test
    void hello() {
        HelloController hello = new HelloController();
        String response= hello.hello();
        assertEquals("Hello World", response);
    }
}