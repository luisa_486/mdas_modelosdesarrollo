package com.lasalle.modelosDesarrollo.helloapp;

import com.lasalle.modelosDesarrollo.helloapp.controllers.HelloController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloAppApplication.class, args);
		System.out.println("The App Start!");
	}

}
