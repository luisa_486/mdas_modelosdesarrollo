package com.lasalle.modelosDesarrollo.helloapp;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HelloAppAutomationTest {
    //We should add @Test annotation that JUnit will run below method
    @Test
    //Start to write our test method.
    public void swTestHelloApp() {

        System.setProperty("webdriver.chrome.driver","C:\\Users\\Acer\\Downloads\\chromedriver.exe");
        //Step 1- Driver Instantiation: Instantiate driver object as ChromeDriver
        WebDriver driver = new ChromeDriver();

        //Step 2- Navigation: Open a website
        driver.navigate().to("http://localhost:8080/hello");

        //Step 3- Assertion: Check its title is correct
        //assertEquals method Parameters: Expected Value, Actual Value, Assertion Message
        assertEquals("Hello World", driver.getTitle(), "Title check failed!");

        //Step 4- Close Driver
        driver.close();

        //Step 5- Quit Driver
        driver.quit();
    }
}
