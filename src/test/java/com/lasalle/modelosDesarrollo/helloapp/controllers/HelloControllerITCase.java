package com.lasalle.modelosDesarrollo.helloapp.controllers;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(HelloController.class)
class HelloControllerITCase {

    @Autowired
    private MockMvc mock;

    @Test
    void hello() throws Exception {
        RequestBuilder request= MockMvcRequestBuilders.get("/hello");
        MvcResult result= mock.perform(request).andReturn();
        assertEquals("Hello World", result.getResponse().getContentAsString());
    }
}