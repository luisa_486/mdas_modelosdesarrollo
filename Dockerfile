FROM openjdk:8
COPY . /tmp
WORKDIR /tmp
EXPOSE 8080
CMD ["java", "-jar", "hello-world-integration-app.jar"]

